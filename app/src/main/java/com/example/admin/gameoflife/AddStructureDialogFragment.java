package com.example.admin.gameoflife;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;


public class AddStructureDialogFragment extends DialogFragment {


    private StructurePickListener listener;

    public void setListener(StructurePickListener listener) {
        this.listener = listener;
    }

    static AddStructureDialogFragment newInstance(int type) {
        AddStructureDialogFragment f = new AddStructureDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("type", type);
        f.setArguments(args);
        return f;

    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final int type = getArguments().getInt("type");

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.pick_structure);


            builder.setItems(setType(type), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            listener.setDrawMode(type,which);

                        }
                    }

            );
        return builder.create();
    }



    private int setType(int type){
        switch (type) {
            case 0:
                return R.array.still_lifes;
            case 1:
                return R.array.oscillators;
            case 2:
                return R.array.spaceships;
            case 3:
                return R.array.guns;
        }

        throw new IndexOutOfBoundsException();
    }


}