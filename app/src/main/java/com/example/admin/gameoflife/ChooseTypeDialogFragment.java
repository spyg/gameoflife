package com.example.admin.gameoflife;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;


public class ChooseTypeDialogFragment extends DialogFragment {


    private StructurePickListener listener;

    public void setListener(StructurePickListener listener) {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.choose_type)
                .setItems(R.array.type_array, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                AddStructureDialogFragment addStructureDialogFragment = AddStructureDialogFragment.newInstance(which);
                                addStructureDialogFragment.setListener(listener);
                                addStructureDialogFragment.show(getActivity().getSupportFragmentManager(), null);
                            }
                        }

                );
        return builder.create();
    }

}