package com.example.admin.gameoflife;


public class Board {

    private Cell[][] cells;
    private int rows;
    private int cols;

    public Board(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        cells = new Cell[rows][cols];
        resetAll();
    }

    public void resetAll() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                cells[i][j] = new Cell(false);
            }
        }
    }

    public void setCellValue(int x, int y, boolean isAlive) {
        if (x >= 0 && y >= 0 && x < rows && y < cols) {
            cells[x][y].setAlive(isAlive);
        }
    }

    public void invertState(int x, int y) {
        if (x >= 0 && y >= 0 && x < rows && y < cols) {
            if (cells[x][y].isAlive()) {
                cells[x][y].setAlive(false);
            } else {
                cells[x][y].setAlive(true);
            }
        }
    }


    public boolean getCellValue(int x, int y) {
        if (x >= 0 && y >= 0 && x < rows && y < cols) {
            return cells[x][y].isAlive();
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public void nextCycle() {

        Cell[][] newBoard = new Cell[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                newBoard[i][j] = cells[i][j].clone();
            }
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                int neighboursCount = countAliveNeighbours(i, j);
                newBoard[i][j].changeState(neighboursCount);
            }
        }
        cells = newBoard;
    }

    public int countAliveNeighbours(int i, int j) {
        int startX = Math.max(i - 1, 0);
        int startY = Math.max(j - 1, 0);
        int endX = Math.min(i + 1, rows - 1);
        int endY = Math.min(j + 1, cols - 1);

        int aliveNeighbours = 0;
        for (int x = startX; x <= endX; x++) {
            for (int y = startY; y <= endY; y++) {

                if (cells[x][y].isAlive()) {
                    aliveNeighbours++;
                }

            }
        }

        if (cells[i][j].isAlive()) {
            aliveNeighbours--;
        }

        return aliveNeighbours;
    }

}
