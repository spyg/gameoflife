package com.example.admin.gameoflife;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity implements StructurePickListener {


    public static final int CELL_SIZE = 5;
    public static final int OPAQUE = 0xFF;
    public static final int TRANSPARENT = 0x30;

    private boolean drawMode;
    private int type;
    private int which;

    private int dpHeight;
    private int dpWidth;
    private LinearLayout mainLinearLayout;
    private Board board;
    private ImageView imageViewBoard[][];
    private int rows;
    private int cols;
    private Timer timer;
    private boolean isAnimationRunning;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainLinearLayout = (LinearLayout) findViewById(R.id.linear_layout_main);

        init();

    }


    private void init() {

        isAnimationRunning = false;
        calculateScreenDimensions();
        board = new Board(rows, cols);
        initImageViewBoard();

    }


    private void createBlinker() {
        board.setCellValue(1, 1, true);
        board.setCellValue(1, 2, true);
        board.setCellValue(1, 3, true);
    }


    private void initImageViewBoard() {

        imageViewBoard = new ImageView[rows][cols];

        for (int i = 0; i < rows; i++) {

            LinearLayout rowLinearLayout = new LinearLayout(this);
            rowLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            rowLinearLayout.setOrientation(LinearLayout.HORIZONTAL);

            for (int j = 0; j < cols; j++) {

                imageViewBoard[i][j] = new ImageView(this);
                imageViewBoard[i][j].setLayoutParams(new LinearLayout.LayoutParams(dpsToPixels(CELL_SIZE), dpsToPixels(CELL_SIZE)));
                imageViewBoard[i][j].setImageResource(R.drawable.circle);
                imageViewBoard[i][j].getDrawable().setAlpha(TRANSPARENT);

                final int finalI = i;
                final int finalJ = j;
                imageViewBoard[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateView(finalI, finalJ);
                    }
                });
                rowLinearLayout.addView(imageViewBoard[i][j]);

            }

            mainLinearLayout.addView(rowLinearLayout);
        }

    }

    private void updateView(int i, int j) {
        if (!drawMode) {
            board.invertState(i, j);
            refreshImageViewBoardCell(i, j);
        } else {
            drawStructure(i, j, selectStructure());
            refreshImageViewBoard();
            drawMode = false;
        }

    }


    private int[][] selectStructure() {
        switch (type) {
            case 0:
                switch (which) {
                    case 0:
                        return Structures.BAKERY;
                }
            case 1:
                switch (which) {
                    case 0:
                        return Structures.PENTADECATHLON;
                    case 1:
                        return Structures.ALMOSYMMETRIC;
                    case 2:
                        return Structures.FONTAINE;

                }
            case 2:
                switch (which) {
                    case 0:
                        return Structures.GLIDER;
                    case 1:
                        return Structures.LIGHTWEIGHT_SPACESHIP;
                    case 2:
                        return Structures.DART;
                }
            case 3:
                switch (which) {
                    case 0:
                        return Structures.GLIDER_GUN;
                }
        }
        return Structures.GLIDER;
    }


    private void drawStructure(int i, int j, int structure[][]) {
        for (int p = 0; p < structure.length; p++) {
            for (int q = 0; q < structure[p].length; q++) {
                board.setCellValue(p + i, q + j, castToBoolean(structure[p][q]));
            }
        }

    }

    private boolean castToBoolean(int i) {
        if (i != 0)
            return true;
        else
            return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_start:
                if (isAnimationRunning) {
                    isAnimationRunning = false;
                    stopAnimation();
                    item.setTitle("start");
                } else {
                    isAnimationRunning = true;
                    startAnimation();
                    item.setTitle("stop");
                }
                return true;
            case R.id.action_reset:
                board.resetAll();
                refreshImageViewBoard();
                return true;

            case R.id.action_add_structure:
                ChooseTypeDialogFragment chooseTypeDialogFragment = new ChooseTypeDialogFragment();
                chooseTypeDialogFragment.setListener(this);
                chooseTypeDialogFragment.show(getSupportFragmentManager(), null);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void stopAnimation() {
        timer.cancel();
        timer.purge();
    }


    private void startAnimation() {
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                board.nextCycle();
                timerMethod();

            }
        }, 100, 100);
    }

    private void timerMethod() {
        //This method is called directly by the timer
        //and runs in the same thread as the timer.

        //We call the method that will work with the UI
        //through the runOnUiThread method.
        this.runOnUiThread(new Runnable() {
                               public void run() {
                                   //This method runs in the same thread as the UI.
                                   //Do something to the UI thread here
                                   refreshImageViewBoard();
                               }
                           }
        );
    }


    private void refreshImageViewBoard() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                refreshImageViewBoardCell(i, j);
            }
        }
    }

    private void refreshImageViewBoardCell(int i, int j) {
        if (board.getCellValue(i, j) == true)
            imageViewBoard[i][j].getDrawable().setAlpha(OPAQUE);
        else
            imageViewBoard[i][j].getDrawable().setAlpha(TRANSPARENT);
    }


    private int dpsToPixels(int dps) {
        final float scale = getResources().getDisplayMetrics().density;
        int pixels = (int) (dps * scale + 0.5f);
        return pixels;
    }

    private void calculateScreenDimensions() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = getResources().getDisplayMetrics().density;
        dpHeight = (int) (outMetrics.heightPixels / density);
        dpWidth = (int) (outMetrics.widthPixels / density);

        cols = dpWidth / CELL_SIZE;
        rows = dpHeight / CELL_SIZE;

    }


    @Override
    public void setDrawMode(int type, int which) {
        drawMode = true;
        this.type = type;
        this.which = which;
    }
}
